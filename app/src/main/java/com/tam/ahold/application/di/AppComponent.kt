package com.tam.ahold.application.di

import com.tam.ahold.application.Application
import com.tam.ahold.core.museumcore.di.MuseumCoreModule
import com.tam.ahold.feature.museum.di.MuseumViewModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        MuseumViewModule::class,
        MuseumCoreModule::class
    ]
)
interface AppComponent : AndroidInjector<Application> {

    @Component.Factory
    interface Builder : AndroidInjector.Factory<Application>
}
