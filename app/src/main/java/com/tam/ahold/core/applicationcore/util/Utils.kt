package com.tam.ahold.core.applicationcore.util

import kotlin.reflect.KMutableProperty0

/**
 * Performs a double check on a property to enforce singleton behavior. Property is instantiated if it is null
 */

fun <T> Any.doubleCheck(property: KMutableProperty0<T?>, initValue: (() -> T)): T {
    val valuePreLock = property.get()
    if (valuePreLock != null) {
        return valuePreLock
    }
    synchronized(this) {
        val valuePostLock = property.get()
        if (valuePostLock != null) {
            return valuePostLock
        }
        return initValue().also {
            property.set(it)
        }
    }
}
