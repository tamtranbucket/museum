package com.tam.ahold.core.museumcore.di

import com.tam.ahold.BuildConfig
import com.tam.ahold.core.applicationcore.util.doubleCheck
import com.tam.ahold.core.museumcore.network.client.MuseumClient
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class MuseumCoreModule {

    private var museumRepository: MuseumRepository? = null

    private val okhttpClientBuilder: OkHttpClient.Builder =
        OkHttpClient.Builder().addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
        )

    @Provides
    @Reusable
    fun provideMuseumRepository(
        museumClient: MuseumClient
    ): MuseumRepository = doubleCheck(this::museumRepository) {
        MuseumRepository(museumClient)
    }

    @Provides
    @Reusable
    fun provideMuseumClient(): MuseumClient {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.MUSEUM_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okhttpClientBuilder.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return retrofit.create(MuseumClient::class.java)
    }
}
