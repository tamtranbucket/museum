package com.tam.ahold.core.museumcore.model

import com.tam.ahold.core.museumcore.network.model.response.detail.MuseumDetailResponseModel

sealed class DetailApiState() {
    object Loading : DetailApiState()
    data class Success(val model: MuseumDetailResponseModel) : DetailApiState()
    data class Error(val throwable: Throwable, val objectNumber: String) : DetailApiState()
}
