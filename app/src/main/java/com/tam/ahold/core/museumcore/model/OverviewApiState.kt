package com.tam.ahold.core.museumcore.model

import com.tam.ahold.core.museumcore.network.model.response.overview.ArtObject
import java.util.LinkedList

sealed class OverviewApiState(open val model: LinkedList<ArtObject>, open val page: Int) {
    data class Loading(override val model: LinkedList<ArtObject>, override val page: Int) : OverviewApiState(model, page)
    data class Success(override val model: LinkedList<ArtObject>, override val page: Int) : OverviewApiState(model, page)
    class Error private constructor(val throwable: Throwable, override val model: LinkedList<ArtObject>, override val page: Int) : OverviewApiState(model, page) {
        companion object {
            fun create(throwable: Throwable, apiState: OverviewApiState): Error = Error(throwable, apiState.model, apiState.page)
        }
    }
}
