package com.tam.ahold.core.museumcore.network.client

import com.tam.ahold.BuildConfig
import com.tam.ahold.core.museumcore.network.model.response.detail.MuseumDetailResponseModel
import com.tam.ahold.core.museumcore.network.model.response.overview.MuseumOverviewResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MuseumClient {
    @GET("/api/nl/collection")
    fun getMuseumOverview(
        @Query(value = "key") clientKey: String = BuildConfig.API_KEY,
        @Query(value = "p") pageNumber: String,
        @Query(value = "ps") resultPerPage: String
    ): Single<MuseumOverviewResponseModel>

    @GET("/api/nl/collection/{objectNumber}")
    fun getMuseumDetail(
        @Path(value = "objectNumber") objectNumber: String,
        @Query(value = "key") clientKey: String = BuildConfig.API_KEY,
    ): Single<MuseumDetailResponseModel>
}
