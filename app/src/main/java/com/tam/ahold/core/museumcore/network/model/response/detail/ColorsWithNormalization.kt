package com.tam.ahold.core.museumcore.network.model.response.detail

import com.google.gson.annotations.SerializedName

data class ColorsWithNormalization(
    @SerializedName("normalizedHex")
    val normalizedHex: String?,
    @SerializedName("originalHex")
    val originalHex: String?
)
