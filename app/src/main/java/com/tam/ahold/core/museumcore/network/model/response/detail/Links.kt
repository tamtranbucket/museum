package com.tam.ahold.core.museumcore.network.model.response.detail

import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("search")
    val search: String?
)
