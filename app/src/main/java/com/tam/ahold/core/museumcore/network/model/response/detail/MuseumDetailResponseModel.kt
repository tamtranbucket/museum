package com.tam.ahold.core.museumcore.network.model.response.detail

import com.google.gson.annotations.SerializedName

data class MuseumDetailResponseModel(
    @SerializedName("artObject")
    val artObject: ArtObject?,
    @SerializedName("artObjectPage")
    val artObjectPage: ArtObjectPage?,
    @SerializedName("elapsedMilliseconds")
    val elapsedMilliseconds: Int?
)
