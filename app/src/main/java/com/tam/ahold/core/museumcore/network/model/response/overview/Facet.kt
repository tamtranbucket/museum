package com.tam.ahold.core.museumcore.network.model.response.overview

import com.google.gson.annotations.SerializedName

data class Facet(
    @SerializedName("facets")
    val facets: List<Facet>?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("otherTerms")
    val otherTerms: Int?,
    @SerializedName("prettyName")
    val prettyName: Int?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("value")
    val value: Int?
)
