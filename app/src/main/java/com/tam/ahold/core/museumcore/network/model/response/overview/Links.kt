package com.tam.ahold.core.museumcore.network.model.response.overview

import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("self")
    val self: String?,
    @SerializedName("web")
    val web: String?
)
