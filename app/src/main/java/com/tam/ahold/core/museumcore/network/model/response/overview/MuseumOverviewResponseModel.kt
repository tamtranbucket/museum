package com.tam.ahold.core.museumcore.network.model.response.overview

import com.google.gson.annotations.SerializedName
import java.util.LinkedList

data class MuseumOverviewResponseModel(
    @SerializedName("artObjects")
    val artObjects: LinkedList<ArtObject>?,
    @SerializedName("count")
    val count: Int?,
    @SerializedName("countFacets")
    val countFacets: CountFacets?,
    @SerializedName("elapsedMilliseconds")
    val elapsedMilliseconds: Int?,
    @SerializedName("facets")
    val facets: List<Facet>?
)
