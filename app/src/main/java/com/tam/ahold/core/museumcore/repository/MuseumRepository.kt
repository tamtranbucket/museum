package com.tam.ahold.core.museumcore.repository

import com.tam.ahold.core.museumcore.model.DetailApiState
import com.tam.ahold.core.museumcore.model.OverviewApiState
import com.tam.ahold.core.museumcore.network.client.MuseumClient
import com.tam.ahold.core.museumcore.network.model.response.detail.MuseumDetailResponseModel
import com.tam.ahold.core.museumcore.network.model.response.overview.ArtObject
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.LinkedList
import javax.inject.Inject

class MuseumRepository @Inject constructor(private val museumClient: MuseumClient) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    companion object {
        private const val RESULT_PER_PAGE: Int = 50
        private const val INITIAL_PAGE = 1
    }

    private val overviewTotalResultState: BehaviorSubject<OverviewApiState> = BehaviorSubject.createDefault(OverviewApiState.Loading(model = LinkedList<ArtObject>(), page = INITIAL_PAGE))
    val overviewTotalResultObservable: Observable<OverviewApiState> = overviewTotalResultState.distinctUntilChanged()

    private val detailState: BehaviorSubject<DetailApiState> = BehaviorSubject.createDefault(DetailApiState.Loading)
    val detailStateObservable: Observable<DetailApiState> = detailState.distinctUntilChanged()

    fun initialLoadOverview() {
        if (overviewTotalResultState.value?.model?.isEmpty() == true) {
            loadOverview()
        }
    }

    fun loadOverview() {
        val pageNumber: Int = overviewTotalResultState.value?.page ?: 0
        museumClient.getMuseumOverview(pageNumber = pageNumber.toString(), resultPerPage = RESULT_PER_PAGE.toString())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    it.artObjects?.let { artObjects ->
                        if (overviewTotalResultState.value?.model?.containsAll(artObjects) == false) {
                            overviewTotalResultState.value?.model?.let { overviewResult -> artObjects.let(overviewResult::addAll) }
                            val nextList = requireNotNull(overviewTotalResultState.value?.model)
                            val nextPage: Int = (overviewTotalResultState.value?.page ?: 0) + 1
                            overviewTotalResultState.onNext(OverviewApiState.Success(nextList, nextPage))
                        }
                    }
                },
                {
                    overviewTotalResultState.onNext(OverviewApiState.Error.create(it, requireNotNull(overviewTotalResultState.value)))
                }
            )
            .addTo(compositeDisposable)
    }

    fun resetDetailState() {
        detailState.onNext(DetailApiState.Loading)
    }

    fun loadDetail(objectNumber: String) {
        museumClient.getMuseumDetail(objectNumber = objectNumber)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { detailState.onNext(DetailApiState.Loading) }
            .subscribe(
                {
                    detailState.onNext(DetailApiState.Success(it))
                },
                {
                    detailState.onNext(DetailApiState.Error(it, objectNumber))
                }
            )
            .addTo(compositeDisposable)
    }

    fun getDetailModel(): MuseumDetailResponseModel? {
        return (detailState.value as? DetailApiState.Success)?.model
    }

    fun clear() {
        overviewTotalResultState.onNext(OverviewApiState.Loading(model = LinkedList<ArtObject>(), page = 1))
        resetDetailState()
    }
}
