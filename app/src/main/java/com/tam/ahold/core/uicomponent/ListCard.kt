package com.tam.ahold.core.uicomponent

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.tam.ahold.R
import com.tam.ahold.databinding.ListCardBinding

class ListCard(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs, 0, R.style.list_card) {

    companion object {

        @BindingAdapter("listcard_image")
        @JvmStatic
        fun setImage(view: ListCard, @DrawableRes image: Int?) {
            view.image = image
        }

        @BindingAdapter("listcard_introtext")
        @JvmStatic
        fun setIntroText(view: ListCard, introText: CharSequence?) {
            view.introText = introText
        }

        @BindingAdapter("listcard_introtext")
        @JvmStatic
        fun setIntroText(view: ListCard, @StringRes introText: Int) {
            if (introText != 0) {
                view.introText = view.context.getString(introText)
            } else {
                view.introText = null
            }
        }

        @BindingAdapter("listcard_maintext")
        @JvmStatic
        fun setMainText(view: ListCard, mainText: CharSequence?) {
            view.mainText = mainText
        }

        @BindingAdapter("listcard_maintext")
        @JvmStatic
        fun setMainText(view: ListCard, @StringRes mainText: Int) {
            if (mainText != 0) {
                view.mainText = view.context.getString(mainText)
            } else {
                view.mainText = null
            }
        }

        @BindingAdapter("listcard_firstbuttontext")
        @JvmStatic
        fun setFirstButtonText(view: ListCard, text: CharSequence?) {
            view.firstButtonText = text
        }

        @BindingAdapter("listcard_firstbuttontext")
        @JvmStatic
        fun setFirstButtonText(view: ListCard, @StringRes text: Int) {
            if (text != 0) {
                view.firstButtonText = view.context.getString(text)
            } else {
                view.firstButtonText = null
            }
        }

        @BindingAdapter("listcard_secondbuttontext")
        @JvmStatic
        fun setSecondButtonText(view: ListCard, text: CharSequence?) {
            view.secondButtonText = text
        }

        @BindingAdapter("listcard_secondbuttontext")
        @JvmStatic
        fun setSecondButtonText(view: ListCard, @StringRes text: Int) {
            if (text != 0) {
                view.secondButtonText = view.context.getString(text)
            } else {
                view.secondButtonText = null
            }
        }

        @BindingAdapter("listcard_imagevisibility")
        @JvmStatic
        fun setImageVisibility(view: ListCard, isVisible: Boolean) {
            view.imageVisibility = isVisible
        }

        @BindingAdapter("listcard_introtextvisibility")
        @JvmStatic
        fun setMainTextVisibility(view: ListCard, isVisible: Boolean) {
            view.mainTextVisibility = isVisible
        }

        @BindingAdapter("listcard_maintextvisibility")
        @JvmStatic
        fun setIntroTextVisibility(view: ListCard, isVisible: Boolean) {
            view.introTextVisibility = isVisible
        }

        @BindingAdapter("listcard_firstbuttonvisibility")
        @JvmStatic
        fun setFirstButtonVisibility(view: ListCard, isVisible: Boolean) {
            view.firstButtonVisibility = isVisible
        }

        @BindingAdapter("listcard_secondbuttonvisibility")
        @JvmStatic
        fun setSecondButtonVisibility(view: ListCard, isVisible: Boolean) {
            view.secondButtonVisibility = isVisible
        }

        @BindingAdapter("listcard_firstbuttononclick")
        @JvmStatic
        fun setFirstButtonListener(
            view: ListCard,
            onClickListenerListCard: ListCardOnclickListener?
        ) {
            view.firstButtonListener = onClickListenerListCard
        }

        @BindingAdapter("listcard_secondbuttononclick")
        @JvmStatic
        fun setSecondButtonListener(
            view: ListCard,
            onClickListenerListCard: ListCardOnclickListener?
        ) {
            view.secondButtonListener = onClickListenerListCard
        }
    }

    private val binding = ListCardBinding.inflate(LayoutInflater.from(context), this, true)

    val imageView: ImageView get() = binding.headerImage

    var image: Int?
        get() = binding.headerImage.tag as? Int
        set(value) {
            if (value != null && value > 0) {
                binding.headerImage.setImageResource(value)
                binding.headerImage.tag = Integer.valueOf(value)
                binding.headerImage.visibility = View.VISIBLE
            } else {
                binding.headerImage.visibility = View.GONE
            }
        }

    var introText: CharSequence?
        get() = binding.introText.text
        set(value) {
            if (!value.isNullOrBlank()) {
                binding.introText.text = value
                introTextVisibility = true
            } else {
                introTextVisibility = false
            }
        }

    var mainText: CharSequence?
        get() = binding.mainText.text
        set(value) {
            if (!value.isNullOrBlank()) {
                binding.mainText.text = value
                mainTextVisibility = true
            } else {
                mainTextVisibility = false
            }
        }

    var firstButtonText: CharSequence?
        get() = binding.firstButton.text
        set(value) {
            if (!value.isNullOrBlank()) {
                binding.firstButton.text = value
                firstButtonVisibility = true
            } else {
                firstButtonVisibility = false
            }
        }

    var secondButtonText: CharSequence?
        get() = binding.secondButton.text
        set(value) {
            if (!value.isNullOrBlank()) {
                binding.secondButton.text = value
                secondButtonVisibility = true
            } else {
                secondButtonVisibility = false
            }
        }

    var imageVisibility: Boolean
        get() = binding.headerImage.visibility == View.VISIBLE
        set(value) {
            binding.headerImage.visibility = if (value) View.VISIBLE else View.GONE
        }

    var introTextVisibility: Boolean
        get() = binding.introText.visibility == View.VISIBLE
        set(value) {
            binding.introText.visibility = if (value) View.VISIBLE else View.GONE
        }

    var mainTextVisibility: Boolean
        get() = binding.mainText.visibility == View.VISIBLE
        set(value) {
            binding.mainText.visibility = if (value) View.VISIBLE else View.GONE
        }

    var firstButtonVisibility: Boolean
        get() = binding.firstButton.visibility == View.VISIBLE
        set(value) {
            binding.firstButton.visibility = if (value) View.VISIBLE else View.GONE
        }

    var secondButtonVisibility: Boolean
        get() = binding.secondButton.visibility == View.VISIBLE
        set(value) {
            binding.secondButton.visibility = if (value) View.VISIBLE else View.GONE
        }

    private var firstButtonListener: ListCardOnclickListener? = null
    private var secondButtonListener: ListCardOnclickListener? = null

    init {
        if (attrs != null) {
            val typedArray =
                context.theme.obtainStyledAttributes(attrs, R.styleable.listcard_attrs, 0, 0)
            image = typedArray.getResourceId(R.styleable.listcard_attrs_listcard_image, -1)
            introText = typedArray.getString(R.styleable.listcard_attrs_listcard_introtext)
            mainText = typedArray.getString(R.styleable.listcard_attrs_listcard_maintext)
            firstButtonText = typedArray.getString(R.styleable.listcard_attrs_listcard_firstbuttontext)
            secondButtonText = typedArray.getString(R.styleable.listcard_attrs_listcard_secondbuttontext)

            val isImageVisible: Boolean = image?.let { it > -1 } ?: false
            val isIntroTextVisible: Boolean = introText?.isNotBlank() ?: false
            val isMainTextVisible: Boolean = mainText?.isNotBlank() ?: false
            val isFirstButtonVisibility: Boolean = firstButtonText?.isNotBlank() ?: false
            val isSecondButtonVisibility: Boolean = secondButtonText?.isNotBlank() ?: false
            imageVisibility = typedArray.getBoolean(R.styleable.listcard_attrs_listcard_imagevisibility, isImageVisible)
            introTextVisibility = typedArray.getBoolean(R.styleable.listcard_attrs_listcard_introtextvisibility, isIntroTextVisible)
            mainTextVisibility = typedArray.getBoolean(R.styleable.listcard_attrs_listcard_maintextvisibility, isMainTextVisible)
            firstButtonVisibility = typedArray.getBoolean(R.styleable.listcard_attrs_listcard_firstbuttonvisibility, isFirstButtonVisibility)
            secondButtonVisibility = typedArray.getBoolean(R.styleable.listcard_attrs_listcard_secondbuttonvisibility, isSecondButtonVisibility)
            typedArray.recycle()
        }

        binding.firstButton.setOnClickListener { firstButtonListener?.onClick() }
        binding.secondButton.setOnClickListener { secondButtonListener?.onClick() }
    }

    interface ListCardOnclickListener {
        fun onClick()
    }
}
