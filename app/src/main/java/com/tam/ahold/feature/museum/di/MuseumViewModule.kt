package com.tam.ahold.feature.museum.di

import com.tam.ahold.feature.museum.view.MuseumActivity
import com.tam.ahold.feature.museum.view.MuseumDetailErrorFragment
import com.tam.ahold.feature.museum.view.MuseumDetailFragment
import com.tam.ahold.feature.museum.view.MuseumDetailLoadingFragment
import com.tam.ahold.feature.museum.view.MuseumLoadingFragment
import com.tam.ahold.feature.museum.view.MuseumOverviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MuseumViewModule {

    @ContributesAndroidInjector
    abstract fun provideMuseumActivity(): MuseumActivity

    @ContributesAndroidInjector
    abstract fun provideMuseumOverviewFragment(): MuseumOverviewFragment

    @ContributesAndroidInjector
    abstract fun provideMuseumLoadingFragment(): MuseumLoadingFragment

    @ContributesAndroidInjector
    abstract fun provideDetailLoadingFragment(): MuseumDetailLoadingFragment

    @ContributesAndroidInjector
    abstract fun provideMuseumDetailFragment(): MuseumDetailFragment

    @ContributesAndroidInjector
    abstract fun provideMuseumDetailErrorFragment(): MuseumDetailErrorFragment
}
