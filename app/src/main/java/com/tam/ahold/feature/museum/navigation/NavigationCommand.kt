package com.tam.ahold.feature.museum.navigation

import android.content.Intent
import androidx.navigation.NavDirections

sealed class NavigationCommand {
    class ToFragment(val directions: NavDirections) : NavigationCommand()
    class ToNavIntent(val intent: Intent) : NavigationCommand()
    object ToPreviousFragment : NavigationCommand()
}
