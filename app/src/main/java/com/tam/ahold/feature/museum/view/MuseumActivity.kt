package com.tam.ahold.feature.museum.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumActivityBinding
import com.tam.ahold.feature.museum.viewmodel.MuseumViewModel
import javax.inject.Inject

class MuseumActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumViewModel>
    val viewModel: MuseumViewModel by viewModels { factory }

    private var _binding: MuseumActivityBinding? = null
    val binding: MuseumActivityBinding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = DataBindingUtil.setContentView<MuseumActivityBinding>(
            this,
            R.layout.museum_activity
        )?.apply {
            this.lifecycleOwner = this@MuseumActivity
        }

        findNavController(R.id.museum_host_fragment).setGraph(R.navigation.museum_nav_graph, intent.extras)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onBackPressed() {
        val currentFragment = getCurrentFragment()
        if (currentFragment?.onBackPressed() == false) {
            super.onBackPressed()
        }
    }

    private fun getCurrentFragment(): MuseumBaseFragment<*>? {
        return supportFragmentManager
            .findFragmentById(R.id.museum_host_fragment)
            ?.childFragmentManager
            ?.primaryNavigationFragment as? MuseumBaseFragment<*>
    }
}
