package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.tam.ahold.R
import com.tam.ahold.feature.museum.navigation.NavigationCommand
import com.tam.ahold.feature.museum.viewmodel.MuseumBaseViewModel
import dagger.android.support.DaggerFragment

abstract class MuseumBaseFragment<VM : MuseumBaseViewModel> : DaggerFragment() {
    abstract val viewModel: VM

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.headerTitle?.let { activity?.title = activity?.getString(it) }

        viewModel.navigationCommand.observe(
            viewLifecycleOwner
        ) {
            when (it) {
                is NavigationCommand.ToFragment -> {
                    val navController: NavController =
                        Navigation.findNavController(requireActivity().findViewById(R.id.museum_host_fragment))

                    if (navController.currentDestination?.getAction(it.directions.actionId) != null) {
                        navController.navigate(it.directions)
                    }
                }
                is NavigationCommand.ToPreviousFragment -> {
                    Navigation
                        .findNavController(requireActivity().findViewById(R.id.museum_host_fragment))
                        .navigateUp()
                }
                is NavigationCommand.ToNavIntent -> {
                    startActivity(it.intent)
                }
            }
        }
    }

    open fun onBackPressed(): Boolean {
        viewModel.onBackPressed()
        return false
    }
}
