package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumErrorFragmentBinding
import com.tam.ahold.feature.museum.viewmodel.MuseumDetailErrorViewModel
import javax.inject.Inject

class MuseumDetailErrorFragment : MuseumBaseFragment<MuseumDetailErrorViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumDetailErrorViewModel>
    override val viewModel: MuseumDetailErrorViewModel by viewModels { factory }

    private var _binding: MuseumErrorFragmentBinding? = null
    private val binding: MuseumErrorFragmentBinding get() = _binding!!

    private val args: MuseumDetailErrorFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<MuseumErrorFragmentBinding>(
            inflater,
            R.layout.museum_error_fragment,
            container,
            false
        ).apply {
            lifecycleOwner = this@MuseumDetailErrorFragment
            viewModel = this@MuseumDetailErrorFragment.viewModel
        }
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.objectNumber = args.objectNumber
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
