package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumDetailFragmentBinding
import com.tam.ahold.feature.museum.viewmodel.MuseumDetailViewModel
import javax.inject.Inject

class MuseumDetailFragment : MuseumBaseFragment<MuseumDetailViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumDetailViewModel>
    override val viewModel: MuseumDetailViewModel by viewModels { factory }

    private var _binding: MuseumDetailFragmentBinding? = null
    private val binding: MuseumDetailFragmentBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<MuseumDetailFragmentBinding>(
            inflater,
            R.layout.museum_detail_fragment,
            container,
            false
        ).apply {
            lifecycleOwner = this@MuseumDetailFragment
            viewModel = this@MuseumDetailFragment.viewModel
        }
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getModelLiveData().observe(viewLifecycleOwner) {
            it.artObject?.principalMaker?.let { principalMaker -> activity?.title = principalMaker }
            it.artObject?.webImage?.url?.let { url ->
                Glide.with(requireContext())
                    .load(url)
                    .into(binding.image)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
