package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumLoadingFragmentBinding
import com.tam.ahold.feature.museum.viewmodel.MuseumDetailLoadingViewModel
import javax.inject.Inject

class MuseumDetailLoadingFragment : MuseumBaseFragment<MuseumDetailLoadingViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumDetailLoadingViewModel>
    override val viewModel: MuseumDetailLoadingViewModel by viewModels { factory }

    private var _binding: MuseumLoadingFragmentBinding? = null
    private val binding: MuseumLoadingFragmentBinding get() = _binding!!

    private val args: MuseumDetailLoadingFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<MuseumLoadingFragmentBinding>(
            inflater,
            R.layout.museum_loading_fragment,
            container,
            false
        ).apply {
            lifecycleOwner = this@MuseumDetailLoadingFragment
        }
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadDetail(args.objectNumber)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
