package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumLoadingFragmentBinding
import com.tam.ahold.feature.museum.viewmodel.MuseumLoadingViewModel
import javax.inject.Inject

class MuseumLoadingFragment : MuseumBaseFragment<MuseumLoadingViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumLoadingViewModel>
    override val viewModel: MuseumLoadingViewModel by viewModels { factory }

    private var _binding: MuseumLoadingFragmentBinding? = null
    private val binding: MuseumLoadingFragmentBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<MuseumLoadingFragmentBinding>(
            inflater,
            R.layout.museum_loading_fragment,
            container,
            false
        ).apply {
            lifecycleOwner = this@MuseumLoadingFragment
        }
        return _binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
