package com.tam.ahold.feature.museum.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.tam.ahold.R
import com.tam.ahold.core.applicationcore.util.ViewModelFactory
import com.tam.ahold.databinding.MuseumOverviewFragmentBinding
import com.tam.ahold.feature.museum.view.adapter.MuseumOverviewRecyclerViewAdapter
import com.tam.ahold.feature.museum.viewmodel.MuseumOverViewViewModel
import javax.inject.Inject

class MuseumOverviewFragment : MuseumBaseFragment<MuseumOverViewViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<MuseumOverViewViewModel>
    override val viewModel: MuseumOverViewViewModel by viewModels { factory }

    private var _binding: MuseumOverviewFragmentBinding? = null
    private val binding: MuseumOverviewFragmentBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<MuseumOverviewFragmentBinding>(
            inflater,
            R.layout.museum_overview_fragment,
            container,
            false
        ).apply {
            lifecycleOwner = this@MuseumOverviewFragment
        }
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getOverviewTotalResultState().observe(viewLifecycleOwner) {
            (binding.museumOverviewList.adapter as MuseumOverviewRecyclerViewAdapter).addToList(it)
        }

        binding.museumOverviewList.apply {
            adapter = MuseumOverviewRecyclerViewAdapter(
                object : MuseumOverviewRecyclerViewAdapter.MuseumOverviewOnClickListener {
                    override fun onWebsiteClick(websiteLink: String) {
                        viewModel.onWebsiteClicked(websiteLink)
                    }

                    override fun onDetailClick(objectId: String) {
                        viewModel.onDetailClicked(objectId)
                    }

                    override fun onLoadNextPage() {
                        viewModel.loadOverView()
                    }
                }
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
