package com.tam.ahold.feature.museum.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tam.ahold.core.museumcore.network.model.response.overview.ArtObject
import com.tam.ahold.core.uicomponent.ListCard
import com.tam.ahold.core.uicomponent.ListCard.Companion.setFirstButtonListener
import com.tam.ahold.core.uicomponent.ListCard.Companion.setSecondButtonListener
import com.tam.ahold.databinding.MuseumOverviewViewHolderBinding

class MuseumOverviewRecyclerViewAdapter(private val museumOverviewOnClickListener: MuseumOverviewOnClickListener) :
    RecyclerView.Adapter<MuseumOverviewRecyclerViewAdapter.ViewHolder>() {

    companion object {
        const val POSITION_BEFORE_END = 25
    }

    private val list: MutableList<ArtObject> = mutableListOf()

    fun addToList(nextList: List<ArtObject>) {
        if (list.size > 0) {
            val nextSubList: MutableList<ArtObject> = nextList.toMutableList().subList(list.lastIndex, nextList.lastIndex)
            list.addAll(nextSubList)
            notifyItemRangeChanged(list.lastIndex, nextList.lastIndex)
        } else {
            list.addAll(nextList)
            notifyItemRangeChanged(0, nextList.lastIndex)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MuseumOverviewViewHolderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent.rootView as ViewGroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
        if ((list.size - POSITION_BEFORE_END) == position) museumOverviewOnClickListener.onLoadNextPage()
    }

    inner class ViewHolder(
        private val binding: MuseumOverviewViewHolderBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(artObject: ArtObject) {
            binding.listCard.apply {
                introText = artObject.title
                mainText = artObject.principalOrFirstMaker
                if (artObject.hasImage == true && !artObject.headerImage?.url.isNullOrBlank()) {
                    binding.listCard.imageVisibility = true
                    Glide.with(binding.root.context)
                        .load(artObject.headerImage?.url)
                        .into(imageView)
                }

                setFirstButtonListener(
                    binding.listCard,
                    object : ListCard.ListCardOnclickListener {
                        override fun onClick() {
                            artObject.objectNumber?.let { museumOverviewOnClickListener.onDetailClick(it) }
                        }
                    }
                )

                setSecondButtonListener(
                    binding.listCard,
                    object : ListCard.ListCardOnclickListener {
                        override fun onClick() {
                            artObject.links?.web?.let { museumOverviewOnClickListener.onWebsiteClick(it) }
                        }
                    }
                )
            }
        }
    }

    interface MuseumOverviewOnClickListener {
        fun onWebsiteClick(websiteLink: String)
        fun onDetailClick(objectId: String)
        fun onLoadNextPage()
    }
}
