package com.tam.ahold.feature.museum.viewmodel

import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.tam.ahold.core.applicationcore.util.SingleLiveEvent
import com.tam.ahold.feature.museum.navigation.NavigationCommand
import io.reactivex.disposables.CompositeDisposable

abstract class MuseumBaseViewModel : ViewModel() {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()
    abstract val headerTitle: Int?
    val navigationCommand = SingleLiveEvent<NavigationCommand>()

    protected fun navigateTo(directions: NavDirections) {
        navigationCommand.value = NavigationCommand.ToFragment(directions)
    }

    protected fun navigateTo(directions: Intent) {
        navigationCommand.value = NavigationCommand.ToNavIntent(directions)
    }

    protected fun navigateToPrevious() {
        navigationCommand.value = NavigationCommand.ToPreviousFragment
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    open fun onBackPressed() {}
}
