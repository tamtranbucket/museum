package com.tam.ahold.feature.museum.viewmodel

import com.tam.ahold.R
import com.tam.ahold.feature.museum.view.MuseumDetailErrorFragmentDirections
import javax.inject.Inject

class MuseumDetailErrorViewModel @Inject constructor() : MuseumBaseViewModel() {
    override val headerTitle: Int = R.string.museum_overview_error_header
    lateinit var objectNumber: String

    fun navigateToDetailLoading() {
        navigateTo(MuseumDetailErrorFragmentDirections.toMuseumDetailLoadingFragment(objectNumber))
    }
}
