package com.tam.ahold.feature.museum.viewmodel

import com.tam.ahold.R
import com.tam.ahold.core.museumcore.model.DetailApiState
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import com.tam.ahold.feature.museum.view.MuseumDetailLoadingFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MuseumDetailLoadingViewModel @Inject constructor(private val museumRepository: MuseumRepository) : MuseumBaseViewModel() {
    override val headerTitle: Int = R.string.app_name

    init {
        museumRepository.detailStateObservable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    is DetailApiState.Success -> {
                        navigateTo(MuseumDetailLoadingFragmentDirections.toMuseumDetailFragment())
                    }
                    is DetailApiState.Error -> {
                        museumRepository.resetDetailState()
                        navigateTo(MuseumDetailLoadingFragmentDirections.toMuseumDetailErrorFragment(it.objectNumber))
                    }
                }
            }.addTo(compositeDisposable)
    }

    fun loadDetail(objectNumber: String) {
        museumRepository.loadDetail(objectNumber)
    }
}
