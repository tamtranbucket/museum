package com.tam.ahold.feature.museum.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tam.ahold.R
import com.tam.ahold.core.museumcore.network.model.response.detail.MuseumDetailResponseModel
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import javax.inject.Inject

class MuseumDetailViewModel @Inject constructor(private val museumRepository: MuseumRepository) : MuseumBaseViewModel() {
    override val headerTitle: Int = R.string.museum_detail_title

    val model: MutableLiveData<MuseumDetailResponseModel> = MutableLiveData()

    init {
        museumRepository.getDetailModel()?.let { model.postValue(it) }
    }

    fun getModelLiveData(): LiveData<MuseumDetailResponseModel> = model

    override fun onBackPressed() {
        navigateToPrevious()
        museumRepository.resetDetailState()
    }
}
