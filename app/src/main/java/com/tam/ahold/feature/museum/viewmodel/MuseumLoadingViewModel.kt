package com.tam.ahold.feature.museum.viewmodel

import com.tam.ahold.R
import com.tam.ahold.core.museumcore.model.OverviewApiState
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import com.tam.ahold.feature.museum.view.MuseumLoadingFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MuseumLoadingViewModel @Inject constructor(private val museumRepository: MuseumRepository) : MuseumBaseViewModel() {
    override val headerTitle: Int get() = R.string.app_name

    init {
        museumRepository.overviewTotalResultObservable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    is OverviewApiState.Success -> {
                        navigateTo(MuseumLoadingFragmentDirections.toMuseumOverviewFragment())
                    }
                }
            }.addTo(compositeDisposable)
        museumRepository.initialLoadOverview()
    }
}
