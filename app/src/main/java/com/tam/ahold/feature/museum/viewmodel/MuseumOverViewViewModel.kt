package com.tam.ahold.feature.museum.viewmodel

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tam.ahold.R
import com.tam.ahold.core.museumcore.model.OverviewApiState
import com.tam.ahold.core.museumcore.network.model.response.overview.ArtObject
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import com.tam.ahold.feature.museum.view.MuseumOverviewFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MuseumOverViewViewModel @Inject constructor(private val museumRepository: MuseumRepository) : MuseumBaseViewModel() {

    override val headerTitle: Int = R.string.museum_overview_title

    private val overviewTotalResult: MutableLiveData<List<ArtObject>> = MutableLiveData()

    init {
        museumRepository.overviewTotalResultObservable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                (it as? OverviewApiState.Success)?.let { state ->
                    overviewTotalResult.postValue(state.model)
                }
            }.addTo(compositeDisposable)
    }

    fun loadOverView() {
        museumRepository.loadOverview()
    }

    fun getOverviewTotalResultState(): LiveData<List<ArtObject>> =
        overviewTotalResult

    fun onDetailClicked(objectNumber: String) {
        navigateTo(MuseumOverviewFragmentDirections.toMuseumDetailLoadingFragment(objectNumber))
    }

    fun onWebsiteClicked(websiteLink: String) {
        navigateTo(
            Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(websiteLink)
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            }
        )
    }
}
