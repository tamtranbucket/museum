package com.tam.ahold.feature.museum.viewmodel

import androidx.lifecycle.ViewModel
import com.tam.ahold.core.museumcore.repository.MuseumRepository
import javax.inject.Inject

class MuseumViewModel @Inject constructor(private val museumRepository: MuseumRepository) : ViewModel() {

    override fun onCleared() {
        museumRepository.clear()
        super.onCleared()
    }
}
